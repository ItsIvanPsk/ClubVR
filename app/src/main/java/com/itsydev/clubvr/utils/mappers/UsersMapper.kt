package com.itsydev.clubvr.utils.mappers

import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.data.models.users.UserDto

fun UserDto.toUserBo(): List<UserBo> {
    return UserBo.map { bo ->
        UserBo(
            name = bo.name,
            password = bo.password,
            surname = bo.surname,
            telf = bo.telf,
            userMail = bo.userMail,
            userRank = bo.userRank,
            user_id = bo.user_id,
            username = bo.username
        )
    }
}