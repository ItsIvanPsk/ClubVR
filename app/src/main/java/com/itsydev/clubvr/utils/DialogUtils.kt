package com.itsydev.clubvr.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

fun <T> showDialog(
    context: Context,
    title: String,
    message: String,
    positiveButtonText: String,
    negativeButtonText: String,
    listener: DialogListener<T>?
) {
    val builder = AlertDialog.Builder(context)
    builder.setTitle(title)
    builder.setMessage(message)
    builder.setPositiveButton(positiveButtonText) { dialog, _ ->
        listener?.onPositiveButtonClicked(dialog)
    }
    builder.setNegativeButton(negativeButtonText) { dialog, _ ->
        listener?.onNegativeButtonClicked(dialog)
    }
    builder.setOnCancelListener { dialog ->
        listener?.onDialogCancelled(dialog)
    }
    builder.setCancelable(true)
    builder.show()
}

interface DialogListener<T> {
    fun onPositiveButtonClicked(dialog: DialogInterface)
    fun onNegativeButtonClicked(dialog: DialogInterface)
    fun onDialogCancelled(dialog: DialogInterface)
}