package com.itsydev.clubvr.utils

import android.annotation.SuppressLint
import android.util.Base64
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class EncryptExtensions {

    companion object {
        const val ALGORITHM = "AES"
        const val TRANSFORMATION = "AES/ECB/PKCS5Padding"
        const val SECRET_KEY = "your-secret-key"
    }

    @SuppressLint("GetInstance")
    fun encrypt(value: String): String {
        val key: Key = SecretKeySpec(SECRET_KEY.toByteArray(), ALGORITHM)
        val cipher: Cipher = Cipher.getInstance(TRANSFORMATION)
        cipher.init(Cipher.ENCRYPT_MODE, key)
        val encryptedValue: ByteArray = cipher.doFinal(value.toByteArray())
        return Base64.encodeToString(encryptedValue, Base64.DEFAULT)
    }

    @SuppressLint("GetInstance")
    fun decrypt(value: String): String {
        val key: Key = SecretKeySpec(SECRET_KEY.toByteArray(), ALGORITHM)
        val cipher: Cipher = Cipher.getInstance(TRANSFORMATION)
        cipher.init(Cipher.DECRYPT_MODE, key)
        val decodedValue: ByteArray = Base64.decode(value, Base64.DEFAULT)
        val decryptedValue: ByteArray = cipher.doFinal(decodedValue)
        return String(decryptedValue)
    }

}