package com.itsydev.clubvr.utils

fun String?.getField(NO_DATA: String = "-"): String {
    return this?.takeIf { it.isNotEmpty() } ?: NO_DATA
}