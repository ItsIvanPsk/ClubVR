package com.itsydev.clubvr.utils

object DateFormats {
    const val FORMAT_1 = "yyyy-MM-dd"
    const val FORMAT_2 = "dd/MM/yyyy"
    const val SHORT_DATE_FORMAT = "MMM dd, yyyy"
    const val FORMAT_4 = "EEEE, MMMM dd, yyyy"
    const val FORMAT_5 = "yyyy-MM-dd HH:mm:ss"
    const val DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss"
    const val FORMAT_7 = "MMM dd, yyyy HH:mm:ss"
    const val FORMAT_8 = "EEEE, MMMM dd, yyyy HH:mm:ss"
}