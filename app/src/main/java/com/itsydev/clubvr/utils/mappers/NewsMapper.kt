package com.itsydev.clubvr.utils.mappers

import com.itsydev.clubvr.data.models.news.NewsBo
import com.itsydev.clubvr.data.models.news.NewsEntity

fun NewsBo.toNewsEntity(): NewsEntity {
    return NewsEntity(
        id.toString(),
        title,
        subtitle,
        body,
        tags.joinToString(separator = ","),
        img.joinToString(separator = ",")
    )
}

fun NewsEntity.toNewsBo(): NewsBo {
    return NewsBo(
        id.toInt(),
        title,
        subtitle,
        body,
        tags.split(","),
        img.split(",")
    )
}