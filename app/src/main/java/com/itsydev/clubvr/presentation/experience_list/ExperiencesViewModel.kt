package com.itsydev.clubvr.presentation.experience_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.experiences.ExperienceDto
import com.itsydev.clubvr.domain.experiences.GetAllExperiencesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExperiencesViewModel @Inject constructor(
    private val getAllExperiencesUseCase: GetAllExperiencesUseCase
) : ViewModel() {

    private var experiences = MutableLiveData<AsyncResult<ExperienceDto>>()

    fun requestExperiences() {
        viewModelScope.launch {
            getAllExperiencesUseCase.getAllExperiences().collect {
                experiences.value = it
            }
        }
    }

    fun getExperiences(): LiveData<AsyncResult<ExperienceDto>> = experiences

}