package com.itsydev.clubvr.presentation.myclub

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MyClubViewModel @Inject constructor(
) : ViewModel()
