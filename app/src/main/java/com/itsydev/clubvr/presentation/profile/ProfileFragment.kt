package com.itsydev.clubvr.presentation.profile

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.itsydev.clubvr.MainActivity
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.databinding.FragmentProfileBinding
import com.itsydev.clubvr.utils.DialogListener
import com.itsydev.clubvr.utils.hide
import com.itsydev.clubvr.utils.show
import com.itsydev.clubvr.utils.showDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment @Inject constructor(

) : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val viewmodel: ProfileViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentProfileBinding.inflate(layoutInflater)
        resetLoginUI()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        (requireActivity() as MainActivity).getActivityBinding().mainFloatingButton.show()
        (requireActivity() as MainActivity).getActivityBinding().bottomAppBar.show()
        setupListeners()

        val sharedPreferences = context?.getSharedPreferences("vr_prefs", Context.MODE_PRIVATE)
        val username = sharedPreferences?.getString("username", null)
        if (username == null) {
            binding.profileBg.hide()
            binding.loginContainer.show()
        } else {
            val password = sharedPreferences.getString("password", null)
            if (!password.isNullOrBlank()) {
                viewmodel.login(username, password)
                setupLoginObserver()
            }
        }

        setupListeners()
        return binding.root
    }

    private fun setupListeners() = with(binding) {
        profileLogin.setOnClickListener {
            setupLoginObserver()
            viewmodel.login(
                binding.profileUsernameField.text.toString(),
                binding.profilePasswordField.text.toString()
            )
        }
        profileLogout.setOnClickListener {
            val logOutListener = object : DialogListener<String> {
                override fun onPositiveButtonClicked(dialog: DialogInterface) {
                    requireContext().getSharedPreferences("vr_prefs", Context.MODE_PRIVATE)?.edit()
                        ?.apply {
                            remove("username")
                            remove("password")
                            remove("user_id")
                            apply()
                        }
                    resetLoginUI()
                    binding.loginContainer.show()
                    binding.profileBg.hide()
                    dialog.dismiss()
                }

                override fun onNegativeButtonClicked(dialog: DialogInterface) {
                    dialog.dismiss()
                }

                override fun onDialogCancelled(dialog: DialogInterface) {
                    dialog.dismiss()
                }
            }

            showDialog(
                requireContext(),
                "Cerrar sesión",
                "¿Deseas cerrar sesión?",
                "Si", "No",
                logOutListener
            )

        }
    }

    private fun resetLoginUI() {
        binding.run {
            profileUsernameField.setText("")
            profilePasswordField.setText("")
            profileSaveUser.isChecked = false
        }
    }

    private fun setupLoginObserver() = with(viewmodel) {
        getUserLogin().observe(viewLifecycleOwner) {
            Log.d("5cos", (it != null).toString())
            Log.d("5cos", it?.logged.toString())
            if (it != null && it.logged) {
                binding.loginContainer.hide()
                binding.profileBg.show()

                if (binding.profileSaveUser.isChecked) {
                    val sharedPreferences =
                        context?.getSharedPreferences("vr_prefs", Context.MODE_PRIVATE)
                    sharedPreferences?.edit()?.apply {
                        putString("username", it.username)
                        putString("password", it.password)
                        putInt("user_id", it.user_id)
                        apply()
                    }
                }

                loadUsernameData(it)
            }
        }
    }

    private fun loadUsernameData(user: UserBo) {
        binding.run {
            profileNameValue.text = user.name
            profileSurnnameValue.text = user.surname
            profileUsernameValue.text = user.username
            profileRankValue.text = user.userRank
            profileContactTelfValue.text = "+34 ${user.telf}"
            profileMailValue.text = user.userMail
        }
    }
}
