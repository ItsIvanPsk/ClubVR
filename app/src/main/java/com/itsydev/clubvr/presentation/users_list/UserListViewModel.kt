package com.itsydev.clubvr.presentation.users_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.data.models.users.UserDto
import com.itsydev.clubvr.domain.users.GetUsersUseCase
import com.itsydev.clubvr.utils.mappers.toUserBo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserListViewModel @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase
) : ViewModel() {

    private var users = MutableLiveData<AsyncResult<UserDto>>()

    fun getUsers(): LiveData<AsyncResult<UserDto>> = users

    fun updateUsers() {
        viewModelScope.launch {
            getUsersUseCase().collect{ userResponse ->
                getUsersUseCase().collect {
                    if (it.data != null) {
                        users.value = it
                    }
                }
             }
        }
    }

}
