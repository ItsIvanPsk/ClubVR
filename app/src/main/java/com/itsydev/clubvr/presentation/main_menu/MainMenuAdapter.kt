package com.itsydev.clubvr.presentation.main_menu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.itsydev.clubvr.data.models.main_menu.MainMenuItemBo
import com.itsydev.clubvr.data.models.news.NewsBo
import com.itsydev.clubvr.databinding.ItemMainMenuBinding

class MainMenuAdapter(
    private val mainMenuItemListener: MainMenuItemListener
) : ListAdapter<NewsBo, MainMenuAdapter.MainMenuItemViewHolder>(MainMenuItemDiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainMenuItemViewHolder {
        val binding =
            ItemMainMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainMenuItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainMenuItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class MainMenuItemViewHolder(private val binding: ItemMainMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NewsBo) = with(binding) {
            mmItemTitle.text = item.title
            mmItemSubtitle.text = item.subtitle
            mmItemTags.text = item.tags.joinToString(separator = ", ")
            mmItemDescription.text = item.body
            mmItemImage.load(item.img[0]) {
                crossfade(true)
            }
            mmItemBg.setOnClickListener {
                mainMenuItemListener.newPressed(it, item.id)
            }
        }
    }
}

object MainMenuItemDiffCallBack : DiffUtil.ItemCallback<NewsBo>() {
    override fun areItemsTheSame(oldItem: NewsBo, newItem: NewsBo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: NewsBo, newItem: NewsBo): Boolean {
        return false
    }
}

interface MainMenuItemListener {
    fun newPressed(view: View, itemId: Int)
}