package com.itsydev.clubvr.presentation.main_menu

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.MainActivity
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.databinding.FragmentMainMenuBinding
import com.itsydev.clubvr.utils.hide
import com.itsydev.clubvr.utils.mappers.toNewsBo
import com.itsydev.clubvr.utils.show
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import javax.inject.Inject

@AndroidEntryPoint
class MainMenuFragment @Inject constructor(

) : Fragment(), MainMenuItemListener {

    private lateinit var binding: FragmentMainMenuBinding
    private val viewmodel: MainMenuViewModel by viewModels()
    private lateinit var adapter: MainMenuAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentMainMenuBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupObservers()
        setupAdapter()
        // checkNewUpdate()
        binding.mmProgress.show()
        (requireActivity() as MainActivity).getActivityBinding().mainFloatingButton.show()
        (requireActivity() as MainActivity).getActivityBinding().bottomAppBar.show()
        return binding.root
    }

    private fun checkNewUpdate() {
        val sharedPreferences =
            requireContext().getSharedPreferences("lastUpdate", Context.MODE_PRIVATE)
        Log.d("5cos", sharedPreferences.getString("lastUpdate", " ").toString())
        viewmodel.checkNewsUpdate(sharedPreferences.getString("lastUpdate", " "))
    }

    private fun setupListeners() = with(binding) { }

    private fun setupAdapter() {
        adapter = MainMenuAdapter(this)
        val recyclerView: RecyclerView = binding.mmRecycler
        recyclerView.adapter = adapter
    }

    private fun setupObservers() {
        viewmodel.getMainMenuItems().observe(viewLifecycleOwner) {
            adapter.submitList(it)
            binding.mmProgress.hide()
            val sharedPreferences =
                requireContext().getSharedPreferences("lastUpdate", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("lastUpdate", Instant.now().toString()).apply()
        }
        viewmodel.getRoomMainMenuItems().observe(viewLifecycleOwner) {
            when (it) {
                is AsyncResult.Error -> {
                    Log.d("5cos", "Error")
                }
                is AsyncResult.Loading -> {
                    Log.d("5cos", "Loading")
                }
                is AsyncResult.Success -> {
                    Log.d("5cos", "Success")
                    it.data?.let { newList ->
                        adapter.submitList(newList.map {
                                newsEntity -> newsEntity.toNewsBo()
                        })
                    }
                    binding.mmProgress.hide()
                }
            }
        }
        viewmodel.getNeedSync().observe(viewLifecycleOwner) {
            if (it) viewmodel.updateMainMenuItems()
        }
    }

    override fun newPressed(view: View, itemId: Int) {
        Log.d("5cos", itemId.toString())
    }

}
