package com.itsydev.clubvr.presentation.main_menu

import android.icu.text.SimpleDateFormat
import android.icu.util.TimeZone
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.news.NewsBo
import com.itsydev.clubvr.data.models.news.NewsEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import org.json.JSONObject
import java.text.ParseException
import java.time.Duration
import java.time.Instant
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class MainMenuViewModel @Inject constructor() : ViewModel() {

    private var firebaseItems = MutableLiveData<List<NewsBo>>()
    private var roomItems = MutableLiveData<AsyncResult<List<NewsEntity>>>()
    private var needSync = MutableLiveData<Boolean>()

    fun updateMainMenuItems() {
        val item_list = mutableListOf<NewsBo>()
        val database = Firebase.firestore
        val collectionReference = database.collection("news")
        collectionReference.get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val data = document.data
                    val img_list = mutableListOf<String>()
                    val tag_list = mutableListOf<String>()
                    val json = JSONObject(data)
                    for (i in 0 until json.getJSONArray("img").length()) {
                        val image = json.getJSONArray("img").get(i)
                        img_list.add(
                            image.toString()
                        )
                    }
                    for (i in 0 until json.getJSONArray("tags").length()) {
                        val tag = json.getJSONArray("tags").get(i)
                        tag_list.add(
                            tag.toString()
                        )
                    }
                    item_list.add(
                        NewsBo(
                            id = data["id"].toString().toInt(),
                            title = data["title"].toString(),
                            subtitle = data["subtitle"].toString(),
                            body = data["body"].toString(),
                            tags = tag_list,
                            img = img_list
                        )
                    )
                }
                Log.d("5cos", item_list.toString())
                firebaseItems.value = item_list
            }
            .addOnFailureListener {
                Log.d("5cos", "Listener failed!!")
            }
    }
    fun getMainMenuItems(): LiveData<List<NewsBo>> = firebaseItems
    fun getRoomMainMenuItems(): LiveData<AsyncResult<List<NewsEntity>>> = roomItems
    fun getNeedSync(): LiveData<Boolean> = needSync

    fun checkNewsUpdate(lastUpdate: String?) {
        if (lastUpdate.isNullOrBlank()) {
            needSync.value = true
            Log.d("5cos", "no last update")
        } else {
            var date = Date()
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
            try {
                date = formatter.parse(lastUpdate)
                formatter.timeZone = TimeZone.getTimeZone("GMT")
                val formattedDate = formatter.format(date)
                println(formattedDate)
            } catch (e: ParseException) {
                println("Error al parsear la fecha: ${e.message}")
            } catch (e: Exception) {
                println("Se ha producido un error inesperado: ${e.message}")
            }
            val instantActual = Instant.now()
            val duracion = Duration.between(date.toInstant(), instantActual)
            needSync.value = duracion.toHours() >= 4
        }
    }
}
