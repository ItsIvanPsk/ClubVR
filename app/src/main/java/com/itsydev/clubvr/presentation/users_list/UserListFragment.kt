package com.itsydev.clubvr.presentation.users_list

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.MainActivity
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.databinding.FragmentUserListBinding
import com.itsydev.clubvr.utils.DialogListener
import com.itsydev.clubvr.utils.hide
import com.itsydev.clubvr.utils.show
import com.itsydev.clubvr.utils.showDialog
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class UserListFragment @Inject constructor(

) : Fragment(), UserDetailListener {

    private lateinit var binding: FragmentUserListBinding
    private val viewmodel: UserListViewModel by viewModels()
    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentUserListBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupUserObserver()
        setupAdapter()
        binding.userListProgress.show()
        (requireActivity() as MainActivity).getActivityBinding().mainFloatingButton.show()
        (requireActivity() as MainActivity).getActivityBinding().bottomAppBar.show()

        return binding.root
    }

    private fun setupAdapter() {
        adapter = UsersAdapter(this)
        val recyclerView: RecyclerView = binding.userUserList
        recyclerView.adapter = adapter
    }

    private fun setupUserObserver() {
        viewmodel.updateUsers()
        viewmodel.getUsers().observe(viewLifecycleOwner){
            when (it) {
                is AsyncResult.Error -> {  }
                is AsyncResult.Loading -> {
                    binding.userListProgress.show()
                    binding.userUserList.hide()
                }
                is AsyncResult.Success -> {
                    binding.userListProgress.hide()
                    binding.userUserList.show()
                    adapter.submitList(it.data?.UserBo)
                }
            }
        }
    }

    override fun showUserData(user: UserBo) {
        val listener = object : DialogListener<String> {
            override fun onPositiveButtonClicked(dialog: DialogInterface) {
                dialog.dismiss()
            }

            override fun onNegativeButtonClicked(dialog: DialogInterface) {
                dialog.dismiss()
            }

            override fun onDialogCancelled(dialog: DialogInterface) {
                dialog.dismiss()
            }
        }

        val formatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        val fechaFormateada = formatter.format(user.joinDate)


        showDialog(
            requireContext(), user.name,
            "Nombre: ${user.name} ${user.surname}\nRol: ${user.userRank}\nEmail: ${user.userMail}\nTelefono: ${user.telf}\nFecha de admisión: " + fechaFormateada,
            "Cerrar", "", listener
        )
    }

}
