package com.itsydev.clubvr.presentation.experience_list

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.ExperiencesActivity
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.databinding.FragmentExperiencesBinding
import com.itsydev.clubvr.utils.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ExperiencesFragment : Fragment(), ExperienceListeners {

    lateinit var binding: FragmentExperiencesBinding
    val viewmodel: ExperiencesViewModel by viewModels()
    private lateinit var adapter: ExperiencesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentExperiencesBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupExperienceAdapter()
        setupExperienceObserver()
        (requireActivity() as ExperiencesActivity).getActivityBinding().experiencesFloatingButton.show()
        (requireActivity() as ExperiencesActivity).getActivityBinding().bottomAppBar.show()
        return binding.root
    }

    private fun setupExperienceAdapter() {
        adapter = ExperiencesAdapter(requireContext(), this)
        val recyclerView: RecyclerView = binding.experiencesRecycler
        recyclerView.adapter = adapter
    }

    private fun setupListeners() = with(binding) {
        experienceSearchValue.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
                if (s.isEmpty()) {

                } else {

                }
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
        binding.experienceFilter.setOnClickListener {

        }
    }

    private fun setupExperienceObserver() {
        viewmodel.requestExperiences()
        viewmodel.getExperiences().observe(viewLifecycleOwner) {
            when (it) {
                is AsyncResult.Error -> {
                    binding.experienceLoading.isVisible = false
                    binding.experiencesRecycler.isVisible = false
                    Log.d("5cos", "err")
                }

                is AsyncResult.Loading -> {
                    binding.experienceLoading.isVisible = true
                    binding.experiencesRecycler.isVisible = false
                }

                is AsyncResult.Success -> {
                    binding.experienceLoading.isVisible = false
                    binding.experiencesRecycler.isVisible = true
                    Log.d("5cos", it.data?.experienceBo.toString())
                    adapter.submitList(it.data?.experienceBo)
                }
            }
        }
    }

    override fun experienceClicked(id: Int) {
        val directions = ExperiencesFragmentDirections.toExperienceDetail(id)
        findNavController().navigate(directions)
    }

}
