package com.itsydev.clubvr.presentation.headset_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.itsydev.clubvr.ExperiencesActivity
import com.itsydev.clubvr.R
import com.itsydev.clubvr.databinding.FragmentHeadsetDetailBinding
import com.itsydev.clubvr.presentation.experience_list.ExperiencesViewModel
import com.itsydev.clubvr.utils.hide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeadsetFragment : Fragment() {

    lateinit var binding: FragmentHeadsetDetailBinding
    val viewmodel: ExperiencesViewModel by activityViewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentHeadsetDetailBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupObservers()
        (requireActivity() as ExperiencesActivity).getActivityBinding().experiencesFloatingButton.hide()
        (requireActivity() as ExperiencesActivity).getActivityBinding().bottomAppBar.hide()
        return binding.root
    }

    private fun setupObservers() {

    }

    private fun setupListeners() {
        binding.headsetDetailGoBack.setOnClickListener {
            it.findNavController().navigate(R.id.action_headsetFragment_to_experienceDetail)
        }
    }

}