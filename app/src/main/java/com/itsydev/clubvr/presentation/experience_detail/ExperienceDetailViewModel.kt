package com.itsydev.clubvr.presentation.experience_detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.experiences.ExperienceDto
import com.itsydev.clubvr.domain.experiences.GetExperienceDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExperienceDetailViewModel @Inject constructor(
    private val getExperienceDataUseCase: GetExperienceDataUseCase
) : ViewModel() {

    private var experience = MutableLiveData<AsyncResult<ExperienceDto>>()

    fun requestExperiences(experience_id: Int) {
        Log.d("5cos", experience_id.toString())
        viewModelScope.launch {
            getExperienceDataUseCase.getExperienceData(experience_id).collect { experiences ->
                Log.d("5cos", experiences.toString())
                if (experiences.data?.experienceBo?.size != 0) {
                    experience.value = experiences
                }
            }
        }
    }

    fun getExperienceData(): LiveData<AsyncResult<ExperienceDto>> = experience

}