package com.itsydev.clubvr.presentation.experience_detail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.itsydev.clubvr.data.models.headsets.HeadsetBo
import com.itsydev.clubvr.databinding.ExperienceHeadsetItemBinding

class HeadSetsAdapter(
    private val context: Context,
    private val listeners: HeadsetsListeners
) : ListAdapter<HeadsetBo, HeadSetsAdapter.HeadsetsViewHolder>(HeadsetsDiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadsetsViewHolder {
        val binding =
            ExperienceHeadsetItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeadsetsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HeadsetsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class HeadsetsViewHolder(private val binding: ExperienceHeadsetItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HeadsetBo) {
            binding.experienceHeadset.setOnClickListener {
                listeners.headsetsClicked(it.id)
            }
            binding.experienceHeadsetName.text = item.name
            binding.experienceHeadsetImage.load(item.img[0].url) {
                crossfade(true)
            }

        }
    }
}

object HeadsetsDiffCallBack : DiffUtil.ItemCallback<HeadsetBo>() {
    override fun areItemsTheSame(oldItem: HeadsetBo, newItem: HeadsetBo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: HeadsetBo, newItem: HeadsetBo): Boolean {
        return false
    }
}

interface HeadsetsListeners {
    fun headsetsClicked(id: Int)
}