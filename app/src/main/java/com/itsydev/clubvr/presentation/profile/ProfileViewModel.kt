package com.itsydev.clubvr.presentation.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.domain.users.LoginUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val loginUserUseCase: LoginUserUseCase
) : ViewModel() {

    private var userLogged = MutableLiveData<UserBo?>()

    fun login(username: String, password: String) {
        viewModelScope.launch {
            loginUserUseCase(username, password).collect { result ->
                if (result.data != null) {
                    userLogged.value = result.data
                }
            }
        }
    }

    fun getUserLogin() = userLogged

}