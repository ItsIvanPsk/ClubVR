package com.itsydev.clubvr.presentation.users_list

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.databinding.ItemUserSimpleBinding

class UsersAdapter(
    private val userDetailListener: UserDetailListener
) : ListAdapter<UserBo, UsersAdapter.UsersItemViewHolder>(UserItemDiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersItemViewHolder {
        val binding =
            ItemUserSimpleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UsersItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class UsersItemViewHolder(private val binding: ItemUserSimpleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserBo) = with(binding) {
            Log.d("5cos", item.toString())
            userItemName.text = item.name
            userItemSurname.text = item.surname
            userItemTitleRank.text = item.userRank
            userItemBg.setOnClickListener {
                userDetailListener.showUserData(item)
            }
        }
    }
}

object UserItemDiffCallBack : DiffUtil.ItemCallback<UserBo>() {
    override fun areItemsTheSame(oldItem: UserBo, newItem: UserBo): Boolean {
        return oldItem.user_id == newItem.user_id
    }

    override fun areContentsTheSame(oldItem: UserBo, newItem: UserBo): Boolean {
        return false
    }
}

interface UserDetailListener {
    fun showUserData(user: UserBo)
}