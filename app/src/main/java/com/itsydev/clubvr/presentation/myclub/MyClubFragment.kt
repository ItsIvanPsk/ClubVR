package com.itsydev.clubvr.presentation.myclub

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.itsydev.clubvr.R
import com.itsydev.clubvr.databinding.FragmentMyclubBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyClubFragment : Fragment() {

    private lateinit var binding: FragmentMyclubBinding
    private val viewmodel: MyClubViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentMyclubBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupObservers()
        return binding.root
    }

    fun setupListeners() = with(binding) {
        myclubButtonRegistersList.setOnClickListener {
            it.findNavController().navigate(R.id.action_myClubFragment_to_registerListFragment)
        }
        myclubButtonRegistersCreate.setOnClickListener {
            //it.findNavController().navigate(R.id.action_myClubFragment2_to_userListFragment)
        }
        myclubButtonClubvrUsers.setOnClickListener {
            it.findNavController().navigate(R.id.action_myClubFragment2_to_userListFragment)
        }
    }

    private fun setupObservers() {

    }

}
