package com.itsydev.clubvr.presentation.register_list

import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.R
import com.itsydev.clubvr.data.models.registers.RegistersBo
import com.itsydev.clubvr.databinding.ItemRegisterSimpleBinding
import com.itsydev.clubvr.databinding.ItemUserSimpleBinding
import com.itsydev.clubvr.utils.DateFormats
import com.itsydev.clubvr.utils.DateFormats.DEFAULT_DATE_FORMAT
import com.itsydev.clubvr.utils.getField
import okhttp3.internal.format
import java.util.*

class RegisterAdapter() : ListAdapter<RegistersBo, RegisterAdapter.RegisterItemViewHolder>(RegisterItemDiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegisterItemViewHolder {
        val binding =
            ItemRegisterSimpleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RegisterItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RegisterItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class RegisterItemViewHolder(private val binding: ItemRegisterSimpleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val dateFormatter = SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.getDefault())

        fun bind(item: RegistersBo) = with(binding) {
            registerUser.text = item.register_user.getField("")
            registerCreationDate.text = dateFormatter.format(item.register_date).getField("")
            registerMessage.text = item.body.getField("")
            if (item.validation_date == Date()) {
                registerValidationDate.text = dateFormatter.format(item.validation_date).getField("")
            }
        }
    }
}

object RegisterItemDiffCallBack : DiffUtil.ItemCallback<RegistersBo>() {
    override fun areItemsTheSame(oldItem: RegistersBo, newItem: RegistersBo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: RegistersBo, newItem: RegistersBo): Boolean {
        return false
    }
}
