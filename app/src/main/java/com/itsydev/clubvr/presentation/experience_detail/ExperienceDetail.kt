package com.itsydev.clubvr.presentation.experience_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import coil.transform.CircleCropTransformation
import com.itsydev.clubvr.ExperiencesActivity
import com.itsydev.clubvr.R
import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.databinding.FragmentExperienceDetailBinding
import com.itsydev.clubvr.utils.hide
import com.itsydev.clubvr.utils.show
import dagger.hilt.android.AndroidEntryPoint
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem
import javax.inject.Inject


@AndroidEntryPoint
class ExperienceDetail @Inject constructor(

) : Fragment(), HeadsetsListeners {

    private lateinit var binding: FragmentExperienceDetailBinding
    private val viewmodel: ExperienceDetailViewModel by viewModels()
    private val args: ExperienceDetailArgs by navArgs()

    private var experience_id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentExperienceDetailBinding.inflate(layoutInflater)
        experience_id = args.experienceId
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupExperienceDataObserver()
        (requireActivity() as ExperiencesActivity).getActivityBinding().experiencesFloatingButton.hide()
        (requireActivity() as ExperiencesActivity).getActivityBinding().bottomAppBar.hide()
        return binding.root
    }

    private fun setupListeners() = with(binding) {
        experienceDetailGoBack.setOnClickListener {
            val directions = ExperienceDetailDirections.goToExperienceList()
            findNavController().navigate(directions)
        }
    }

    private fun setupExperienceDataObserver() = with(viewmodel) {
        requestExperiences(experience_id)
        getExperienceData().observe(viewLifecycleOwner) {
            when (it) {
                is AsyncResult.Error -> {

                }

                is AsyncResult.Loading -> {
                    binding.experienceProgresss.show()
                    binding.experienceBg.hide()
                }

                is AsyncResult.Success -> {
                    binding.experienceProgresss.hide()
                    binding.experienceBg.show()
                    it.data?.experienceBo?.get(0)?.let { experience ->
                        binding.experienceDetailHeaderGameName.text = experience.name
                        binding.experienceDetailDescriptionValue.text = experience.description
                        binding.experienceDetailHeaderGameIcon.load(experience.experience_images[0].src) {
                            crossfade(true)
                            transformations(CircleCropTransformation())
                        }
                        val starRating = StringBuilder()
                        for (i in 0 until experience.rating) {
                            starRating.append("\u2605")
                        }
                        for (i in experience.rating until 5) {
                            starRating.append("\u2606")
                        }
                        binding.experienceDetailRatingLabel.text =
                            "${resources.getString(R.string.experience_detail_rating)} $starRating"
                        val photoList = experience.experience_images.map { image ->
                            CarouselItem(
                                image.src,
                                ""
                            )
                        }.toList()
                        binding.experienceDetailImagesCarousel.setData(photoList)
                    }
                }
            }
        }
    }

    override fun headsetsClicked(id: Int) {
        TODO("Not yet implemented")
    }

}
