package com.itsydev.clubvr.presentation.register_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.itsydev.clubvr.MainActivity
import com.itsydev.clubvr.databinding.FragmentRegisterListBinding
import com.itsydev.clubvr.utils.show
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterListFragment @Inject constructor(

) : Fragment() {

    private lateinit var binding: FragmentRegisterListBinding
    private val viewmodel: RegisterListViewModel by viewModels()
    private lateinit var adapter: RegisterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentRegisterListBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupListeners()
        setupObservers()
        setupAdapter()
        binding.registerListProgress.show()
        (requireActivity() as MainActivity).getActivityBinding().mainFloatingButton.show()
        (requireActivity() as MainActivity).getActivityBinding().bottomAppBar.show()
        return binding.root
    }

    private fun setupListeners() = with(binding) { }

    private fun setupAdapter() {
        adapter = RegisterAdapter()
        val recyclerView: RecyclerView = binding.registerList
        recyclerView.adapter = adapter
    }

    private fun setupObservers() {

    }

}
