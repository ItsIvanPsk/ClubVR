package com.itsydev.clubvr.presentation.experience_list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.itsydev.clubvr.data.models.experiences.ExperienceBo
import com.itsydev.clubvr.databinding.ItemLayoutExperienceBinding

class ExperiencesAdapter(
    private val context: Context,
    private val experienceListeners: ExperienceListeners
) : ListAdapter<ExperienceBo, ExperiencesAdapter.ExperiencesViewHolder>(ExperiencesDiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExperiencesViewHolder {
        val binding =
            ItemLayoutExperienceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ExperiencesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ExperiencesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ExperiencesViewHolder(private val binding: ItemLayoutExperienceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ExperienceBo) {
            binding.experienceName.text = item.name
            binding.experienceCategory.text = item.experience_category.joinToString(separator = ",")
            binding.experienceImage.load(item.experience_images[0].src) {
                crossfade(true)
                transformations(CircleCropTransformation())
            }
            binding.experienceBg.setOnClickListener {
                experienceListeners.experienceClicked(item.experience_id)
            }
        }
    }
}

object ExperiencesDiffCallBack : DiffUtil.ItemCallback<ExperienceBo>() {
    override fun areItemsTheSame(oldItem: ExperienceBo, newItem: ExperienceBo): Boolean {
        return oldItem.experience_id == newItem.experience_id
    }

    override fun areContentsTheSame(oldItem: ExperienceBo, newItem: ExperienceBo): Boolean {
        return false
    }
}

interface ExperienceListeners {
    fun experienceClicked(id: Int)
}