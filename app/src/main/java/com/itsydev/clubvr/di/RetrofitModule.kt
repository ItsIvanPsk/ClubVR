package com.itsydev.clubvr.di

import com.itsydev.clubvr.data.VRAPI
import com.itsydev.clubvr.data.common.AuthInterceptor
import com.itsydev.clubvr.data.common.Constants
import com.itsydev.clubvr.domain.users.GetUsersUseCase
import com.itsydev.clubvr.domain.users.GetUsersUseCaseImpl
import com.itsydev.clubvr.domain.users.LoginUserUseCase
import com.itsydev.clubvr.domain.users.LoginUserUseCaseImpl
import com.itsydev.clubvr.domain.users.UserRepository
import com.itsydev.clubvr.domain.users.UserRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RetrofitModule {

    @Singleton
    @Provides
    fun providesRetrofit(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }

    @Singleton
    @Provides
    fun providesOkHttpClient(interceptor: AuthInterceptor): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Singleton
    @Provides
    fun providesVRAPI(retrofitBuilder: Retrofit.Builder): VRAPI {
        return retrofitBuilder.build().create(VRAPI::class.java)
    }

    @Singleton
    @Provides
    fun userRepositoryProvider(api: VRAPI) = UserRepositoryImpl(api) as UserRepository

    @Singleton
    @Provides
    fun getUsersUseCaseProvider(repository: UserRepository) = GetUsersUseCaseImpl(repository) as GetUsersUseCase

    @Singleton
    @Provides
    fun loginUserUseCaseProvider(repository: UserRepository) = LoginUserUseCaseImpl(repository) as LoginUserUseCase


}