package com.itsydev.clubvr.di

import com.itsydev.clubvr.data.VRAPI
import com.itsydev.clubvr.domain.experiences.ExperienceRepository
import com.itsydev.clubvr.domain.experiences.ExperienceRepositoryImpl
import com.itsydev.clubvr.domain.experiences.GetAllExperiencesUseCase
import com.itsydev.clubvr.domain.experiences.GetAllExperiencesUseCaseImpl
import com.itsydev.clubvr.domain.experiences.GetExperienceDataUseCase
import com.itsydev.clubvr.domain.experiences.GetExperienceDataUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ExperiencesModule {

    @Singleton
    @Provides
    fun experienceRepositoryProvider(api: VRAPI) =
        ExperienceRepositoryImpl(api) as ExperienceRepository

    @Singleton
    @Provides
    fun getAllExperiencesProvider(repository: ExperienceRepository) =
        GetAllExperiencesUseCaseImpl(repository) as GetAllExperiencesUseCase

    @Singleton
    @Provides
    fun getExperienceDataProvider(repository: ExperienceRepository) =
        GetExperienceDataUseCaseImpl(repository) as GetExperienceDataUseCase

}