package com.itsydev.clubvr.data.common

object Constants {
    const val BASE_URL = "https://apivr-production.up.railway.app/"
    const val USER_TOKEN = "user_token"
    const val PREFS_TOKEN_FILE = "prefs_token_file"
}