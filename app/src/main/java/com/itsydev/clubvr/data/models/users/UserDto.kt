package com.itsydev.clubvr.data.models.users

data class UserDto(
    val UserBo: List<UserBo> = emptyList()
)

data class UserBo(
    val user_id: Int = 0,
    val name: String = "",
    val surname: String = "",
    val username: String = "",
    val password: String = "",
    val userMail: String = "",
    val userRank: String = "",
    val joinDate: String = "",
    val logged: Boolean = false,
    val telf: Int = 0,
    val rPerm: Int = 0,
    val wPerm: Int = 0,
    val fPerm: Int = 0,
    val tPerm: Int = 0
)

data class LoginRequest(
    val username: String,
    val password: String
)
