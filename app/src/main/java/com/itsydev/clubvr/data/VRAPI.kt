package com.itsydev.clubvr.data

import com.itsydev.clubvr.data.models.experiences.ExperienceDto
import com.itsydev.clubvr.data.models.users.LoginRequest
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.data.models.users.UserDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface VRAPI {

    @GET("api/users/get_user_data/{user_id}")
    suspend fun getUser(@Path("user_id") user_id: Int): UserDto

    @POST("api/users/login_users")
    suspend fun loginUser(@Body request: LoginRequest): UserBo

    @POST("api/users/get_users_list")
    suspend fun getAllUsers(): UserDto

    @POST("api/experiences/get_experience_list")
    suspend fun getAllExperiences(): ExperienceDto

    @GET("api/experiences/get_experience/{experience_id}")
    suspend fun getExperienceData(@Path("experience_id") experience_id: Int): ExperienceDto

}
