package com.itsydev.clubvr.data.models.registers

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.Date

data class Registers (
    val id: Int
)

@Entity
data class RegisterEntity (
    @PrimaryKey(autoGenerate = false) @SerializedName("id") val id: String = "",
    @SerializedName("type") @ColumnInfo var type: Int = 0,
    @SerializedName("body") @ColumnInfo var body: String = "",
    @SerializedName("register_date") @ColumnInfo var register_date: String = "",
    @SerializedName("validation_date") @ColumnInfo var validation_date: String = "",
    @SerializedName("register_user") @ColumnInfo var register_user: String = "",
    @SerializedName("validation_user") @ColumnInfo val validation_user: String = ""
)

data class RegistersBo (
    val id: Int,
    val type: Int,
    val body: String,
    val register_user: String = "",
    val register_date: Date = Date(),
    val validation_user: String = "",
    val validation_date: Date = Date()
)