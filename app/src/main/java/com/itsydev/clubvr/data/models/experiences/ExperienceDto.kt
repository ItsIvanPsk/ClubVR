package com.itsydev.clubvr.data.models.experiences

data class ExperienceDto(
    val experienceBo: List<ExperienceBo> = emptyList()
)

data class ExperienceBo(
    val experience_id: Int = 0,
    val name: String = "",
    val rating: Int = 0,
    val description: String = "",
    val experience_category: List<ExperienceCategory> = emptyList(),
    val experience_images: List<ExperienceImage> = emptyList(),
    val experience_warnings: List<ExperienceWarnings> = emptyList()
)

data class ExperienceImage(
    val src: String
)

data class ExperienceWarnings(
    val title: String
)

data class ExperienceCategory(
    val categoryName: String
)
