package com.itsydev.clubvr.data.models.main_menu

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

data class MainMenuItem(
    val id: Int
)

data class MainMenuItemBo(
    val id: Int,
    val title: String,
    val subtitle: String,
    val body: String,
    val img: List<String>,
    val tags: List<String>
)

@Entity
data class MainMenuItemEntity(
    @PrimaryKey(autoGenerate = false) @SerializedName("id") @ColumnInfo val id: Int,
    @SerializedName("title") @ColumnInfo val title: String,
    @SerializedName("subtitle") @ColumnInfo val subtitle: String,
    @SerializedName("body") @ColumnInfo val body: String,
    @SerializedName("img") @ColumnInfo val img: List<String>,
    @SerializedName("tags") @ColumnInfo val tags: List<String>,
    @SerializedName("fetchDate") @ColumnInfo val fetchDate: Date
)
