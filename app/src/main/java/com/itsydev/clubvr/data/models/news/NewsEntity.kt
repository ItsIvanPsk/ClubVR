package com.itsydev.clubvr.data.models.news

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class NewsEntity(
    @PrimaryKey(autoGenerate = false) @SerializedName("id") val id: String = "",
    @SerializedName("title") @ColumnInfo var title: String = "",
    @SerializedName("subtitle") @ColumnInfo var subtitle: String = "",
    @SerializedName("body") @ColumnInfo var body: String = "",
    @SerializedName("tags") @ColumnInfo var tags: String = "",
    @SerializedName("img") @ColumnInfo val img: String
)

data class NewsBo(
    val id: Int = 0,
    var title: String = "",
    var subtitle: String = "",
    var body: String = "",
    var tags: List<String>,
    val img: List<String>
)