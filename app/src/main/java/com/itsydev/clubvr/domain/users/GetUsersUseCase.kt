package com.itsydev.clubvr.domain.users

import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.data.models.users.UserDto
import kotlinx.coroutines.flow.Flow

interface GetUsersUseCase {
    suspend operator fun invoke() : Flow<AsyncResult<UserDto>>
}

class GetUsersUseCaseImpl(
    private val usersRepository: UserRepository
) : GetUsersUseCase {

    override suspend fun invoke(): Flow<AsyncResult<UserDto>> {
        return usersRepository.getAllUsers().flow()
    }
}