package com.itsydev.clubvr.domain.experiences

import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.experiences.ExperienceDto
import kotlinx.coroutines.flow.Flow

interface GetExperienceDataUseCase {
    suspend fun getExperienceData(id: Int): Flow<AsyncResult<ExperienceDto>>
}

class GetExperienceDataUseCaseImpl(
    private val repository: ExperienceRepository
) : GetExperienceDataUseCase {

    override suspend fun getExperienceData(id: Int): Flow<AsyncResult<ExperienceDto>> {
        return repository.getExperienceData(id).flow()
    }
}