package com.itsydev.clubvr.domain.experiences

import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.experiences.ExperienceDto
import kotlinx.coroutines.flow.Flow

interface GetAllExperiencesUseCase {
    suspend fun getAllExperiences(): Flow<AsyncResult<ExperienceDto>>
}

class GetAllExperiencesUseCaseImpl(
    private val repository: ExperienceRepository
) : GetAllExperiencesUseCase {

    override suspend fun getAllExperiences(): Flow<AsyncResult<ExperienceDto>> {
        return repository.getAllExperiences().flow()
    }
}