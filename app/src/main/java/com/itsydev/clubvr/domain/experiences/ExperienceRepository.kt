package com.itsydev.clubvr.domain.experiences

import android.util.Log
import com.itsydev.clubvr.data.VRAPI
import com.itsydev.clubvr.data.common.RepositoryResponse
import com.itsydev.clubvr.data.common.remoteResponse
import com.itsydev.clubvr.data.models.experiences.ExperienceDto

interface ExperienceRepository {
    suspend fun getAllExperiences(): RepositoryResponse<ExperienceDto>
    suspend fun getExperienceData(experience_id: Int): RepositoryResponse<ExperienceDto>
}

class ExperienceRepositoryImpl(private val api: VRAPI) : ExperienceRepository {

    override suspend fun getAllExperiences(): RepositoryResponse<ExperienceDto> {
        return remoteResponse { api.getAllExperiences() }
    }

    override suspend fun getExperienceData(experience_id: Int): RepositoryResponse<ExperienceDto> {
        return remoteResponse {
            Log.d("5cos", experience_id.toString())
            val data = api.getExperienceData(experience_id)
            Log.d("5cos", data.toString())
            data
        }
    }

}