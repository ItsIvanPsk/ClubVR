package com.itsydev.clubvr.domain.users

import com.itsydev.clubvr.data.common.AsyncResult
import com.itsydev.clubvr.data.models.users.UserBo
import kotlinx.coroutines.flow.Flow

interface LoginUserUseCase {
    suspend operator fun invoke(username: String, password: String): Flow<AsyncResult<UserBo>>
}

class LoginUserUseCaseImpl(
    private val usersRepository: UserRepository
) : LoginUserUseCase {

    override suspend operator fun invoke(username: String, password: String): Flow<AsyncResult<UserBo>> {
        return usersRepository.loginUser(username, password).flow()
    }

}