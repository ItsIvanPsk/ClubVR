package com.itsydev.clubvr.domain.users

import android.util.Log
import com.itsydev.clubvr.data.VRAPI
import com.itsydev.clubvr.data.common.RepositoryResponse
import com.itsydev.clubvr.data.common.remoteResponse
import com.itsydev.clubvr.data.models.users.LoginRequest
import com.itsydev.clubvr.data.models.users.UserBo
import com.itsydev.clubvr.data.models.users.UserDto

interface UserRepository {
    suspend fun getAllUsers(): RepositoryResponse<UserDto>
    suspend fun loginUser(username: String, password: String): RepositoryResponse<UserBo>
}

class UserRepositoryImpl(private val api: VRAPI) : UserRepository {
    override suspend fun getAllUsers(): RepositoryResponse<UserDto> {
        return remoteResponse { api.getAllUsers() }
    }

    override suspend fun loginUser(username: String, password: String): RepositoryResponse<UserBo> {
        return remoteResponse {
            val data = api.loginUser(LoginRequest(username, password))
            Log.d("5cos", data.toString())
            data
        }
    }
}